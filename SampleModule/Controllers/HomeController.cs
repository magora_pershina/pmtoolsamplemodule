﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.BLL.Common.Models.User;
using Autofac;
using BLL;
using BLL.Abstract;
using BLL.Concrete;
using BLL.Validators;
using Common.ApiModels;
using Common.ApiModels.Login;
using Common.Concrete.Helpers;
using Common.Models.Error;
using SampleModule.Filters;

namespace SampleModule.Controllers
{
    public class HomeController : Controller
    {
        [Validator(ValidationKeys.AuthRequest)]
        public ActionResult Index(SessionTokenModel model)
        {
            var helper = IoC.Instance.Resolve<IApiHelper>();
            var user = helper.GetUserModel(model.Token, model.SessionId);
            CurrentUser.Initialize(user);
            return View();
        }

        public ActionResult Error(ErrorModel model)
        {
            return View("Error", model);
        } 
	}
}