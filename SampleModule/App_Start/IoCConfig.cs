﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using App.BLL.Validators;
using BLL.Concrete;
using Common.Concrete;
using Common.Concrete.Helpers;
using RedmineExt.DB.Concrete;

namespace Web.App_Start
{
    public class IoCConfig
    {
        public static void Initialize()
        {
            IoC.Initialize(
                new Bll_IoCModule(),
                new Common_IoCModule(),
                new DAL_IoCModule(),
                new ValidatorIoCModule()
                );
        }
    }
}