﻿using System;
using System.Configuration;
using System.Net;
using System.Web;
using System.Web.Mvc;
using App.BLL.Common.Models.User;
using Autofac;
using BLL;
using BLL.Abstract;
using BLL.Abstract.App.BLL.Common.Abstract;
using Common.Concrete.Helpers;

namespace SampleModule.Filters
{
    #region Usings

    

    #endregion

    public sealed class ExAuthorizeAttribute : AuthorizeAttribute
    {
        #region Instance Fields

        //private readonly ResponseHelper responseHelper;
        private ILogger _logger = IoC.Instance.Resolve<ILoggerFactory>().Get("ProjectPassport");
        #endregion

        #region Constructors

        public ExAuthorizeAttribute()
        {
            //this.responseHelper = new ResponseHelper();
        }

        #endregion

        #region Overridden Methods

        //protected override bool AuthorizeCore(HttpContextBase httpContext)
        //{
        //    var accessTokenHelper = IoC.Instance.Resolve<IApiAccessTokenHelper>();
        //    var cookie = httpContext.Request.Cookies["access_token"];
        //    if (cookie != null && !CurrentUser.IsLoggedIn)
        //    {
        //        _logger.Debug("[AuthorizeAttribute]CurrentUser==null, cookie:{0}",cookie.Value);

        //        var userHelper = IoC.Instance.Resolve<IUserHelper>();
        //        try
        //        {
                    
        //            var accessToken = JsonConvert.DeserializeObject<ApiAccessTokenModel>(cookie.Value);
        //            var accessTokenModel =
        //                accessTokenHelper.DAL_Repository.GetNotExpiratedAccessToken(accessToken.AccessToken, true);
        //            _logger.Debug("[AuthorizeAttribute]Actual access token model exist:{0}", accessTokenModel != null);
        //            if (accessTokenModel == null)
        //            {
        //                _logger.Debug("[AuthorizeAttribute]Refreshing Token Begin");
        //                var url = AppSettingsHelper.Instance.AppSettings[Settings.BaseApiServiceUrl].Value +
        //                          String.Format("/RefreshToken?apiKey={0}&refreshToken={1}&callback={2}",
        //                              ConfigurationManager.AppSettings["apiKey"], accessToken.RefreshToken, "");

        //                using (var webClient = new WebClient())
        //                {
                            
        //                    var response =
        //                        JsonConvert.DeserializeObject<ResultInfo<OAuthResponseModel>>(
        //                            webClient.DownloadString(url));
        //                    _logger.Debug("[AuthorizeAttribute] Refresh response received");
        //                    accessToken = new ApiAccessTokenModel
        //                    {
        //                        AccessToken = response.Data.AccessToken,
        //                        RefreshToken = response.Data.RefreshToken
        //                    };
        //                    _logger.Debug("[AuthorizeAttribute] New AccessToken:{0}", accessToken.AccessToken);
        //                    cookie = new HttpCookie("access_token", JsonConvert.SerializeObject(accessToken))
        //                    {
        //                        Expires = DateTime.Now.AddDays(1)
                                
        //                    };
        //                    httpContext.Response.Cookies.Set(cookie);
        //                    accessTokenModel =
        //                        accessTokenHelper.DAL_Repository.GetNotExpiratedAccessToken(accessToken.AccessToken,
        //                            true);
        //                }
        //            }
        //            _logger.Debug("[AuthorizeAttribute] Get User by access token:{0}", accessToken.AccessToken);
        //            var user = userHelper.DAL_Repository.User_GetById(accessTokenModel.UserId);
        //            _logger.Debug("[AuthorizeAttribute] User By AccessToken:{0}", user.FullName);
        //            user.Groups = userHelper.DAL_Repository.User_GetGroups(user.Id).Select(x => x.Id);
        //            _logger.Debug("[AuthorizeAttribute] User {0} Groups:{1}", user.FullName, String.Join(",",user.Groups));
        //            var systemAccess = AccessHelper.GetSystemAccess(user.Id);

        //            if (user != null)
        //            {
        //                CurrentUser.Info =  new UserSessionModel
        //                {
        //                    UserModel = user,
        //                    SystemAccess = systemAccess
        //                };
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.Debug("[AuthorizeAttribute] exception occured");
        //            var response = httpContext.Response;

        //            cookie.Expires = DateTime.Now.AddDays(-1);
        //            response.Cookies.Set(cookie);
        //        }
        //    }
        //    else
        //    {
                
        //        if (cookie == null && CurrentUser.IsLoggedIn)
        //        {
        //            _logger.Debug("[AuthorizeAttribute] cookie is null and Current User is not null");
        //            return false;
        //        }
        //    }
        //    return CurrentUser.IsLoggedIn;
        //}

        ////
        //protected override void HandleUnauthorizedRequest(AuthorizationContext context)
        //{
        //    if(context.HttpContext.Request.IsAjaxRequest())
        //    {
        //       // context.Result = this.responseHelper.Get_JSONUrlRedirect(this.GenerateRedirectUrl(context.RequestContext));
        //    }
        //    else
        //    {

        //        var redirecresult =
        //            String.Format("{0}?api_key={1}&redirect_url={2}",
        //                AppSettingsHelper.Instance.AppSettings[Settings.OAuthLoginUrl].Value,
        //                ConfigurationManager.AppSettings["apiKey"],
        //                AppSettingsHelper.Instance.AppSettings[Settings.ProjectPassportUrl].Value + "/Application/Login");

        //        _logger.Debug(redirecresult);

        //        context.Result = new RedirectResult(redirecresult);
        //    }
        //}

        #endregion

        #region private

        //private string GenerateRedirectUrl(RequestContext context)
        //{
        //    var urlHelper = new UrlHelper(context);
        //    var result = urlHelper.Action("Login", "CommonUser");

        //    if (!string.IsNullOrWhiteSpace(this.Controller) &&
        //       !string.IsNullOrWhiteSpace(this.Action))
        //    {
        //        result = urlHelper.Action(this.Action, this.Controller);
        //    }
        //    else if (!string.IsNullOrWhiteSpace(this.URL))
        //    {
        //        result = this.URL;
        //    }

        //    return result;
        //}

        #endregion

        #region Properties

        public string Action
        {
            get;
            set;
        }

        public string Controller
        {
            get;
            set;
        }

        public object[] RouteValues
        {
            get;
            set;
        }

        public string URL
        {
            get;
            set;
        }

        #endregion
    } //end class
}