﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using BLL.Validators;
using Common.ApiModels;
using Common.Concrete.Helpers;
using Common.Models.Error;
using Common.Models.Validator;
using FluentValidation;
using Newtonsoft.Json.Linq;

namespace SampleModule.Filters
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public class ValidatorAttribute : ActionFilterAttribute
    {
        private readonly ValidationKeys _key;
        public ValidatorAttribute(ValidationKeys key)
        {
            _key = key;
        }

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            var validator = IoC.Instance.ResolveKeyed<IValidator>(_key);
            var validationResult = validator.Validate(actionContext.ActionParameters.First().Value);
            if (!validationResult.IsValid)
            {
                var errorModel = new ErrorModel
                {
                    CodeStr = String.Join(", ", validationResult.Errors.Select(x => x.CustomState)),
                    Text = String.Join(", ", validationResult.Errors.Select(x => String.Format("{0}" + (!String.IsNullOrEmpty(x.PropertyName)?"('{1}')":""), x.ErrorMessage, x.PropertyName)))
                };

                actionContext.Result = new RedirectResult(GenerateRedirectUrl(actionContext.RequestContext, errorModel));
                
            }
        }

        private string GenerateRedirectUrl(RequestContext context, ErrorModel error)
        {

            var urlHelper = new UrlHelper(context);
            var result = urlHelper.Action("Error", "Home", error);

            return result;
        }

    }
}