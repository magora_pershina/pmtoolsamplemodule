﻿using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using BLL.Abstract;
using BLL.Abstract.App.BLL.Common.Abstract;
using BLL.Concrete;
using Common.Concrete.Helpers;

namespace Web.Filters
{
    #region Usings

    

    #endregion

    public sealed class ExCheckAccess : AuthorizeAttribute
    {
        #region Instance Fields

        //private readonly ResponseHelper responseHelper;
        private ILogger _logger = IoC.Instance.Resolve<ILoggerFactory>().Get("ProjectPassport");

        #endregion

        #region Constructors

        public ExCheckAccess(CRUD_Groups group, CRUD_Entities entity, CRUD_Operations crudOperation)
        {
            this.Group = group;
            this.Entity = entity;
            this.UserAction = crudOperation;

            this.responseHelper = new ResponseHelper();
        }

        public ExCheckAccess(CRUD_Groups[] groups)
        {
            this.Groups = groups;

            this.responseHelper = new ResponseHelper();
        }

        public ExCheckAccess(CRUD_Entities[] entitys, CRUD_Operations crudOperation)
        {
            this.Entitys = entitys;
            this.UserAction = crudOperation;
            this.responseHelper = new ResponseHelper();
        }

        #endregion

        #region Overridden Methods

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            _logger.Debug("Try to access {0}", httpContext.Request.Url);

            if (Groups != null &&
                this.Groups.Any())
            {

                var res = CurrentUser.IsLoggedIn ? CurrentUser.IsUserInGroups(Groups) : CurrentUser.IsLoggedIn;
                if (CurrentUser.IsLoggedIn)
                    _logger.Debug("CurrentUser.IsUserInGroups({0}) {1}", String.Join(",", Groups.Select(x=>x.ToString())), CurrentUser.IsUserInGroups(Groups));
                return res;
            }
            else if (Entitys != null &&
                     Entitys.Any())
            {
                if (CurrentUser.IsLoggedIn)
                    _logger.Debug("CurrentUser.IsUserHaveAccessTo({0}, {1}) {2}", String.Join(",", Entitys.Select(x => x.ToString())), UserAction.ToString(), CurrentUser.IsUserHaveAccessTo(Entitys, UserAction));
                return CurrentUser.IsLoggedIn ? CurrentUser.IsUserHaveAccessTo(Entitys, UserAction) : CurrentUser.IsLoggedIn;
            }
            else
            {
                if (CurrentUser.IsLoggedIn)
                    _logger.Debug("CurrentUser.GetAccessByGroup({0}, {1}, {2}): {3}", this.Group, this.Entity, (int)this.UserAction, CurrentUser.GetAccessByGroup((int)this.Group, (int)this.Entity, (int)this.UserAction));
                return CurrentUser.IsLoggedIn
                    ? CurrentUser.GetAccessByGroup((int)this.Group, (int)this.Entity, (int)this.UserAction)
                    : CurrentUser.IsLoggedIn;
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext context)
        {
            if (CurrentUser.IsLoggedIn)
            {
                if (context.HttpContext.Request.IsAjaxRequest())
                {
                    context.Result = this.responseHelper.Get_JSONError("Access Denied");
                }
                else
                {
                    if (context.HttpContext.Request.Url == null) return;
                    context.Result = new RedirectResult(String.Format("/Application/AccessDenied"));
                }
            }
        }

        #endregion

        #region Properties

        public string Action
        {
            get;
            set;
        }

        public string Controller
        {
            get;
            set;
        }

        public bool IsRedirectUrlExists
        {
            get
            {
                return (this.Controller != null && this.Action != null);
            }
        }

        public object[] RouteValues
        {
            get;
            set;
        }

        public CRUD_Groups[] Groups { get; set; }
        public CRUD_Entities[] Entitys { get; set; }

        public string URL
        {
            get;
            set;
        }


        private CRUD_Entities Entity
        {
            get;
            set;
        }

        private CRUD_Groups Group
        {
            get;
            set;
        }

        private CRUD_Operations UserAction
        {
            get;
            set;
        }

        #endregion

        #region Methods

        private string GenerateRedirectUrl(RequestContext context)
        {
            var urlHelper = new UrlHelper(context);
            var result = urlHelper.Action("Login", "CommonUser");

            if (!string.IsNullOrWhiteSpace(this.Controller) &&
               !string.IsNullOrWhiteSpace(this.Action))
            {
                result = urlHelper.Action(this.Action, this.Controller);
            }
            else if (!string.IsNullOrWhiteSpace(this.URL))
            {
                result = this.URL;
            }

            return result;
        }

        #endregion
    } //end class
}