﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;
using Autofac;
using BLL.Abstract;
using BLL.Abstract.App.BLL.Common.Abstract;
using BLL.Concrete;
using Common.Abstract;
using Common.Concrete.Helpers;
using Common.Enums;
using Common.Models.Message;
using NLog.Internal;
using Web.App_Start;

namespace SampleModule
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private ILogger _logger;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            IoCConfig.Initialize();
            ErrorHelper.Instance.Init();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Response.Clear();
            HttpException httpException = exception as HttpException;

            string action = String.Empty;
            if (httpException != null)
            {
                switch (httpException.GetHttpCode())
                {
                    case 404:
                        // page not found
                        action = "HttpError404";
                        break;
                    case 500:
                        // server error
                        action = "HttpError500";
                        break;
                    default:
                        action = "General";
                        break;
                }
            }

            bool isAjaxCall = string.Equals("XMLHttpRequest", Context.Request.Headers["x-requested-with"], StringComparison.OrdinalIgnoreCase);
            Server.ClearError();
            if (isAjaxCall)
            {
                if (_logger == null)
                    _logger = IoC.Instance.Resolve<ILoggerFactory>().Get(System.Configuration.ConfigurationManager.AppSettings["ModuleName"]);

                _logger.Error(LoggerHelper.GetMessageFromException(exception));
                Response.ContentType = "application/json";
                Context.Response.StatusCode = 200;
                Response.Write(
                    new JavaScriptSerializer().Serialize(
                        new MessageModel
                        {
                            MessageType = (int)Enums.e_MessageType.Error,
                            Text = "Server error occured"
                        }
                    )
                );
            }
            else
            {
                _logger.Error(LoggerHelper.GetMessageFromException(exception));
                Server.ClearError();
                Response.Redirect(String.Format("~/Application/Error/?Text=[{0}]{1}", action, exception.Message));
            }


        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("ru-ru");
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("ru-ru");
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            var response = HttpContext.Current.Response;
            var request = HttpContext.Current.Request;

            _logger = IoC.Instance.Resolve<ILoggerFactory>().Get(System.Configuration.ConfigurationManager.AppSettings["ModuleName"]);
            _logger.Info(LoggerHelper.GetResponseInfo(request, response));
        }
    }
}
