﻿using System;
using System.Runtime.Serialization;

namespace Common.Concrete.Exceptions
{
    public class ApiException : System.Exception
    {
        public ApiException()
            : base()
        {
        }

        public ApiException(string message)
            : base(message)
        {
        }

        public ApiException(string format, params object[] args)
            : base(string.Format(format, args))
        {
        }

        public ApiException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public ApiException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException)
        {
        }

        protected ApiException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}