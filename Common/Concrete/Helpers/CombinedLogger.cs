﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLL.Abstract.App.BLL.Common.Abstract;
using Common.Enums;


namespace BLL.Concrete
{
    internal sealed class CombinedLogger : ILogger
    {
        #region private

        private readonly IEnumerable<ILogger> _loggers;

        #endregion

        #region public

        public CombinedLogger(params ILogger[] loggers)
        {
            this._loggers = loggers;
        }

        public CombinedLogger(IEnumerable<ILogger> loggers)
        {
            if (loggers == null)
            {
                throw new ArgumentNullException("loggers");
            }

            this._loggers = loggers;
        }

        #endregion

        #region Interfaces

        #region ILogger Members

        public void Debug(string message)
        {
            foreach (var logger in this._loggers)
            {
                logger.Debug(message);
            }
        }

        public void Debug(string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Debug(formatMessage, arguments);
            }
        }

        public void Debug(IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Debug(provider, formatMessage, arguments);
            }
        }

        public void Error(string message)
        {
            foreach (var logger in this._loggers)
            {
                logger.Error(message);
            }
        }

        public void Error(string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Error(formatMessage, arguments);
            }
        }

        public void Error(IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Error(provider, formatMessage, arguments);
            }
        }

        public void Fatal(string message)
        {
            foreach (var logger in this._loggers)
            {
                logger.Fatal(message);
            }
        }

        public void Fatal(string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Fatal(formatMessage, arguments);
            }
        }

        public void Fatal(IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Fatal(provider, formatMessage, arguments);
            }
        }

        public void Info(string message)
        {
            foreach (var logger in this._loggers)
            {
                logger.Info(message);
            }
        }

        public void Info(string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Info(formatMessage, arguments);
            }
        }

        public void Info(IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Info(provider, formatMessage, arguments);
            }
        }

        public void Trace(string message)
        {
            foreach (var logger in this._loggers)
            {
                logger.Trace(message);
            }
        }

        public void Trace(string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Trace(formatMessage, arguments);
            }
        }

        public void Trace(IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Trace(provider, formatMessage, arguments);
            }
        }

        public void Warn(string message)
        {
            foreach (var logger in this._loggers)
            {
                logger.Warn(message);
            }
        }

        public void Warn(string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Warn(formatMessage, arguments);
            }
        }

        public void Warn(IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Warn(provider, formatMessage, arguments);
            }
        }

        public void Write(Enums.e_LogLevel level, string message)
        {
            foreach (var logger in this._loggers)
            {
                logger.Write(level, message);
            }
        }

        public void Write(Enums.e_LogLevel level, string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Write(level, formatMessage, arguments);
            }
        }

        public void Write(Enums.e_LogLevel level, IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            foreach (var logger in this._loggers)
            {
                logger.Write(level, provider, formatMessage, arguments);
            }
        }

        #endregion

        #endregion
    }
}
