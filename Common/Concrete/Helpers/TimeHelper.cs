﻿using System;
using System.Collections.Generic;

namespace BLL.Concrete
{
    public class TimeHelper
    {
        public static DateTime GetStartWeekDate(DateTime date)
        {
            var startWeekDate = date.AddDays(-1 * ((int)(date.DayOfWeek)-1)).Date;
            if (date.DayOfWeek == DayOfWeek.Sunday)
                startWeekDate = startWeekDate.AddDays(-7);
            return startWeekDate;
        }

        public static DateTime GetStartWeekDateStatic(DateTime date)
        {
            var startWeekDate = date.AddDays(-1 * ((int)(date.DayOfWeek)-1)).Date;
            if (date.DayOfWeek == DayOfWeek.Sunday)
                startWeekDate = startWeekDate.AddDays(-7);
            return startWeekDate;
        }

        public static long GetTimestamp(DateTime date)
        {
            return (long) (date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        public static DateTime GetDateFromTimestamp(long date)
        {
            var dtDateTime = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds( date ).ToLocalTime();
            return dtDateTime;
        }

        public IEnumerable<DateTime> GetStartWeekDatesInMonth(DateTime date)
        {
            var mdate = new DateTime(date.Year, date.Month, 1);
            var weeks = new List<DateTime>();
            var startweek = GetStartWeekDate(mdate);
            
            weeks.Add(startweek);
            var currentDate = startweek.AddDays(7);

            while (currentDate < mdate.AddMonths(1))
            {
                weeks.Add(currentDate);
                currentDate = currentDate.AddDays(7);
            }

            return weeks;
        }
    }
}
