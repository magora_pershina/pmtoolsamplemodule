﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Common.Models.Error;
using Newtonsoft.Json;

namespace Common.Concrete.Helpers
{
    public class ErrorHelper
    {

        private static ErrorHelper _instance;
        public Dictionary<string, string> Errors;

        private ErrorHelper()
        {
            Errors = new Dictionary<string, string>();
        }

        public static ErrorHelper Instance
        {
            get { return _instance ?? (_instance = new ErrorHelper()); }
        }

        public void Init()
        {
            var path = System.Configuration.ConfigurationManager.AppSettings["ErrorTablePath"];
            var absolutePath = HttpContext.Current.Server.MapPath(path);

            using (StreamReader r = new StreamReader(absolutePath))
            {
                string json = r.ReadToEnd();
                Errors = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            }

        }

        public static ErrorModel GetModelByCode(int code)
        {
            return new ErrorModel
            {
                Code = code,
                Text = Instance.Errors[code.ToString()]
            };
        }

        public static ErrorModel GetModelByCode(string code)
        {
            return new ErrorModel
            {
                CodeStr = code,
                Text = Instance.Errors[code]
            };
        }

        public static ErrorModel GetModelByEnum(Enums.Enums.Error error)
        {
            return new ErrorModel
            {
                CodeStr = error.ToDisplayName(),
                Text = Instance.Errors[error.ToDisplayName()]
            };
        }
    }
}
