﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Common.ApiModels;
using Common.Concrete.Exceptions;
using Common.Models.Error;
using Newtonsoft.Json;

namespace Common.Concrete.Helpers
{
    public class RequestHelper
    {
        public static string Send(string url, string json, Enums.Enums.RequestType requestType)
        {

            var response = string.Empty;
            using (var client = new WebClient())
            {
                client.Headers[HttpRequestHeader.ContentType] = "application/json";
                response = client.UploadString(url, requestType.ToString(), json);
            }

            var responseModel = JsonConvert.DeserializeObject<dynamic>(response);

            if (responseModel.code != 0)
            {
                var errorResponse = JsonConvert.DeserializeObject<BaseErrorResponseModel>(response);
                var errorModel = new ErrorModel
                {
                    CodeStr = errorResponse.Code,
                    Text = errorResponse.Message
                };
                throw new ApiException(errorModel.Text);
            }

            return response;
        }
    }
}
