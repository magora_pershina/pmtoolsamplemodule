﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using NLog.Internal;

namespace Common.Concrete.Helpers
{
    public class CryptoHelper
    {
        public static string PrivateKey
        {
            get
            {
                var base64 =  System.Configuration.ConfigurationManager.AppSettings["PrivateKey"];
                var bytes = Convert.FromBase64String(base64);
                var bytestr = Encoding.Unicode.GetString(bytes);
                return bytestr;
            }
        }

        public static string PublicKey
        {
            get
            {
                var base64 = System.Configuration.ConfigurationManager.AppSettings["PublicKey"];
                var bytes = Convert.FromBase64String(base64);
                var bytestr = Encoding.Unicode.GetString(bytes);
                return bytestr;
            }
        }

        public static string PublicKeyBase64
        {
            get
            {
                var base64 = System.Configuration.ConfigurationManager.AppSettings["PublicKey"];

                return base64;
            }
        }

        public static string Encryption(string strText)
        {
            var testData = Encoding.UTF8.GetBytes(strText);

            using (var rsa = new RSACryptoServiceProvider())
            {
                try
                {
                    // client encrypting data with public key issued by server                    
                    rsa.FromXmlString(PublicKey.ToString());

                    var encryptedData = rsa.Encrypt(testData, false);

                    var base64Encrypted = Convert.ToBase64String(encryptedData);

                    return base64Encrypted;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }

        public static string Decryption(string strText)
        {

            using (var rsa = new RSACryptoServiceProvider())
            {
                try
                {
                    var base64Encrypted = strText;
                    // server decrypting data with private key                    
                    rsa.FromXmlString(PrivateKey);

                    var resultBytes = Convert.FromBase64String(base64Encrypted);
                    var decryptedBytes = rsa.Decrypt(resultBytes, false);
                    var decryptedData = Encoding.UTF8.GetString(decryptedBytes);
                    return decryptedData.ToString();
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
        }
    }
}
