﻿using System;
using System.Linq;
using BLL.Abstract.App.BLL.Common.Abstract;
using BLL.Concrete;
using Common.Abstract;

namespace Common.Concrete.Helpers
{
    public class LoggerFactory : ILoggerFactory
    {
        #region Interfaces

        #region ILoggerFactory Members

        public ILogger Get(params string[] loggerNames)
        {
            if (loggerNames == null ||
               loggerNames.Length == 0)
            {
                throw new ArgumentNullException("loggerNames");
            }

            if (loggerNames.Length == 1)
            {
                return new Logger(loggerNames[0]);
            }

            var loggers = loggerNames.Select(item => new Logger(item)).ToArray();
            return new CombinedLogger(loggers);
        }

        #endregion

        #endregion
    }
}
