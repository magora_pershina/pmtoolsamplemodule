﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enums;
using NLog;
using ILogger = BLL.Abstract.App.BLL.Common.Abstract.ILogger;

namespace BLL.Concrete
{
    public class Logger : ILogger
    {
        #region private

        private readonly NLog.Logger _logger;

        #endregion

        #region public

        public Logger(string loggerName)
        {
            this._logger = LogManager.GetLogger(loggerName);
        }

        #endregion

        #region Interfaces

        #region ILogger Members

        public void Debug(string message)
        {
            this._logger.Debug(message);
        }

        public void Debug(string formatMessage, params object[] arguments)
        {
            this._logger.Debug(formatMessage, arguments);
        }

        public void Debug(IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            this._logger.Debug(provider, formatMessage, arguments);
        }

        public void Error(string message)
        {
            this._logger.Error(message);
        }

        public void Error(string formatMessage, params object[] arguments)
        {
            this._logger.Error(formatMessage, arguments);
        }

        public void Error(IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            this._logger.Error(provider, formatMessage, arguments);
        }

        public void Fatal(string message)
        {
            this._logger.Fatal(message);
        }

        public void Fatal(string formatMessage, params object[] arguments)
        {
            this._logger.Fatal(formatMessage, arguments);
        }

        public void Fatal(IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            this._logger.Fatal(provider, formatMessage, arguments);
        }

        public void Info(string message)
        {
            this._logger.Info(message);
        }

        public void Info(string formatMessage, params object[] arguments)
        {
            this._logger.Info(formatMessage, arguments);
        }

        public void Info(IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            this._logger.Info(provider, formatMessage, arguments);
        }

        public void Trace(string message)
        {
            this._logger.Trace(message);
        }

        public void Trace(string formatMessage, params object[] arguments)
        {
            this._logger.Trace(formatMessage, arguments);
        }

        public void Trace(IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            this._logger.Trace(provider, formatMessage, arguments);
        }

        public void Warn(string message)
        {
            this._logger.Warn(message);
        }

        public void Warn(string formatMessage, params object[] arguments)
        {
            this._logger.Warn(formatMessage, arguments);
        }

        public void Warn(IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            this._logger.Warn(provider, formatMessage, arguments);
        }

        public void Write(Enums.e_LogLevel level, string message)
        {
            switch (level)
            {
                case Enums.e_LogLevel.Debug:
                    this.Debug(message);
                    break;
                case Enums.e_LogLevel.Error:
                    this.Error(message);
                    break;
                case Enums.e_LogLevel.Fatal:
                    this.Fatal(message);
                    break;
                case Enums.e_LogLevel.Info:
                    this.Info(message);
                    break;
                case Enums.e_LogLevel.Trace:
                    this.Trace(message);
                    break;
                case Enums.e_LogLevel.Warn:
                    this.Warn(message);
                    break;
            }
        }

        public void Write(Enums.e_LogLevel level, string formatMessage, params object[] arguments)
        {
            switch (level)
            {
                case Enums.e_LogLevel.Debug:
                    this.Debug(formatMessage, arguments);
                    break;
                case Enums.e_LogLevel.Error:
                    this.Error(formatMessage, arguments);
                    break;
                case Enums.e_LogLevel.Fatal:
                    this.Fatal(formatMessage, arguments);
                    break;
                case Enums.e_LogLevel.Info:
                    this.Info(formatMessage, arguments);
                    break;
                case Enums.e_LogLevel.Trace:
                    this.Trace(formatMessage, arguments);
                    break;
                case Enums.e_LogLevel.Warn:
                    this.Warn(formatMessage, arguments);
                    break;
            }
        }

        public void Write(Enums.e_LogLevel level, IFormatProvider provider, string formatMessage, params object[] arguments)
        {
            switch (level)
            {
                case Enums.e_LogLevel.Debug:
                    this.Debug(provider, formatMessage, arguments);
                    break;
                case Enums.e_LogLevel.Error:
                    this.Error(provider, formatMessage, arguments);
                    break;
                case Enums.e_LogLevel.Fatal:
                    this.Fatal(provider, formatMessage, arguments);
                    break;
                case Enums.e_LogLevel.Info:
                    this.Info(provider, formatMessage, arguments);
                    break;
                case Enums.e_LogLevel.Trace:
                    this.Trace(provider, formatMessage, arguments);
                    break;
                case Enums.e_LogLevel.Warn:
                    this.Warn(provider, formatMessage, arguments);
                    break;
            }
        }

        #endregion

        #endregion
    }
}
