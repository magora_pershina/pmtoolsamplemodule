﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BLL.Concrete
{
    public class LoggerHelper
    {
        public static string GetMessageFromException(Exception ex)
        {
            Exception logException = ex;
            if (ex.InnerException != null)
                logException = ex.InnerException;

            string strErrorMsg = Environment.NewLine + "[EXCEPTION]" + Environment.NewLine + "Error in Path :" + System.Web.HttpContext.Current.Request.Path;

            // Get the QueryString along with the Virtual Path
            strErrorMsg += Environment.NewLine + "Raw Url :" + System.Web.HttpContext.Current.Request.RawUrl;

            // Get the error message
            strErrorMsg += Environment.NewLine + "Message :" + logException.Message;

            // Source of the message
            strErrorMsg += Environment.NewLine + "Source :" + logException.Source;

            // Stack Trace of the error

            strErrorMsg += Environment.NewLine + "Stack Trace :" + logException.StackTrace;

            // Method where the error occurred
            strErrorMsg += Environment.NewLine + "TargetSite :" + logException.TargetSite;

            return strErrorMsg;
        }

        public static string GetRequestInfo(HttpRequest request)
        {
            string str = Environment.NewLine + "++[REQUEST]++" +  Environment.NewLine + "HttpMethod :" + request.HttpMethod;

            // Get the QueryString along with the Virtual Path
            str += Environment.NewLine + "Raw Url :" + request.RawUrl;

            // Get the error message
            str += Environment.NewLine + "Parameters :" + String.Join(Environment.NewLine, request.Params.AllKeys.Where(x => !x.Contains("ReportViewerWebControl") && !x.Contains("ALL_RAW")).Select(x => string.Format("[{0}]:{1}", x, request.Params.Get(x))));

            // Stack Trace of the error

           // str += Environment.NewLine + "Headers :" + String.Join(",", request.Headers.AllKeys.Select(x => string.Format("{0}:{1}", x, request.Headers.Get(x))));

            // Method where the error occurred
            //str += Environment.NewLine + "QueryString :" + String.Join(",", request.QueryString.AllKeys.Select(x => string.Format("{0}:{1}", x, request.QueryString.Get(x))));

            return str;
        }

        public static string GetResponseInfo(HttpRequest request, HttpResponse response)
        {
            string str = Environment.NewLine + "[[RESPONSE]]" + Environment.NewLine + "ContentType :" + response.ContentType;

            str += Environment.NewLine + "Request url :" + request.RawUrl;

            str += Environment.NewLine + "Parameters :" + String.Join(Environment.NewLine, request.Params.AllKeys.Where(x => !x.Contains("ReportViewerWebControl") && !x.Contains("ALL_RAW") && !x.Contains("ALL_HTTP")).Select(x => string.Format("[{0}]:{1}", x, request.Params.Get(x))));

            // Get the QueryString along with the Virtual Path
            str += Environment.NewLine + "Is Request Being Redirected :" + response.IsRequestBeingRedirected;

            // Get the error message
            str += Environment.NewLine + "Redirect Location :" + response.RedirectLocation;

            // Source of the message
            str += Environment.NewLine + "Status :" + response.Status;

            //
            str += Environment.NewLine + "StatusCode :" + response.StatusCode;

            //
            str += Environment.NewLine + "StatusDescription :" + response.StatusDescription;
            //
            str += Environment.NewLine + "SubStatus Code :" + response.SubStatusCode;

            var filter = response.Filter as ResponseSniffer;

            if (filter != null)
            {
                var reader = new StreamReader(new MemoryStream(filter.RecordStream.GetBuffer()));
                var source = reader.ReadToEnd();
                str += Environment.NewLine + "OutputStream :" + source;
            }

            
            
            // Stack Trace of the error

            str += Environment.NewLine + "Headers :" + String.Join(",", response.Headers.AllKeys.Select(x => string.Format("{0}:{1}", x, response.Headers.Get(x))));

            
            return str;
        }
    }
}
