﻿using Autofac;
using BLL.Abstract;
using BLL.Abstract.App.BLL.Common.Abstract;
using BLL.Concrete;
using Common.Abstract;
using Common.Concrete.Helpers;

namespace Common.Concrete
{
    public class Common_IoCModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<HashHelper>().As<IHashHelper>();
            builder.RegisterType<LoggerFactory>().As<ILoggerFactory>();

            base.Load(builder);
        }
    }//end class
}
