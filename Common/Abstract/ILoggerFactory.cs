﻿using BLL.Abstract.App.BLL.Common.Abstract;

namespace Common.Abstract
{
    public interface ILoggerFactory
    {
        #region public

        ILogger Get(params string[] loggerNames);

        #endregion
    }
}
