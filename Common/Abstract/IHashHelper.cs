﻿namespace Common.Abstract
{
    public interface IHashHelper
    {
        #region Abstract Methods

        string GetMd5Hash(string input);

        string GetSHA256Hash(string input);

        string GetSHA512Hash(string input);

        string GetSalt();

        string GetSaltPassword(string password, string salt);
        string GetSalt(string data, string salt);

        #endregion
    }
}