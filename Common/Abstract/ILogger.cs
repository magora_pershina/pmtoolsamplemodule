﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Enums;

namespace BLL.Abstract
{
    namespace App.BLL.Common.Abstract
    {
        public interface ILogger
        {
            #region public

            void Debug(string message);

            void Debug(string formatMessage, params object[] arguments);

            void Debug(IFormatProvider provider, string formatMessage, params object[] arguments);

            void Error(string message);

            void Error(string formatMessage, params object[] arguments);

            void Error(IFormatProvider provider, string formatMessage, params object[] arguments);

            void Fatal(string message);

            void Fatal(string formatMessage, params object[] arguments);

            void Fatal(IFormatProvider provider, string formatMessage, params object[] arguments);

            void Info(string message);

            void Info(string formatMessage, params object[] arguments);

            void Info(IFormatProvider provider, string formatMessage, params object[] arguments);

            void Trace(string message);

            void Trace(string formatMessage, params object[] arguments);

            void Trace(IFormatProvider provider, string formatMessage, params object[] arguments);

            void Warn(string message);

            void Warn(string formatMessage, params object[] arguments);

            void Warn(IFormatProvider provider, string formatMessage, params object[] arguments);

            void Write(Enums.e_LogLevel level, string message);

            void Write(Enums.e_LogLevel level, string formatMessage, params object[] arguments);

            void Write(Enums.e_LogLevel level, IFormatProvider provider, string formatMessage, params object[] arguments);

            #endregion
        }
    }
}
