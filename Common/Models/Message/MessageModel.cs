﻿namespace Common.Models.Message
{
    public class MessageModel
    {
        public int MessageType { set; get; }
        public string Text { set; get; }
        public string Data { set; get; }
    }
}