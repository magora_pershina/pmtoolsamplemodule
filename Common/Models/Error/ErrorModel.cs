﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Error
{
    public class ErrorModel
    {
        public int Code { set; get; }
        public string CodeStr { set; get; }
        public string Text { set; get; }
    }
}
