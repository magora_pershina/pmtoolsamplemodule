﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models.Validator
{
    public class ApiDescription
    {
        public ApiCode Code { get; set; }
        public string Description { get; set; }
        public string Field { get; set; }
    }

    public class ApiCode
    {
        public string Code { get; set; }
        public HttpStatusCode HttpCode { get; set; }
    }
}
