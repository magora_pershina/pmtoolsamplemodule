﻿using System;

namespace Common.Models
{
    public class BaseModel
    {
        public int Id { set; get; }
        public DateTime CreateDate { set; get; }
        public DateTime? UpdateDate { set; get; }
        public int CreateUserId { set; get; }
        public int? UpdateUserId { set; get; }
        public bool IsActive { set; get; }
        public bool IsNew { get { return Id == 0; }
    }
    }
}
