﻿using System;
using System.Runtime.Serialization;
using Common.ApiModels.Login;
using Common.Models;

namespace App.BLL.Common.Models.User
{
    #region Usings

    using System.Collections.Generic;

    #endregion

    [Serializable]
    public sealed class UserSessionModel
    {
        #region Properties

        public bool IsExpirated
        {
            get;
            set;
        }

        public KeyValuePair<int, KeyValuePair<int, KeyValuePair<int, bool>[]>[]>[] SystemAccess
        {
            get;
            set;
        }

        public UserModel UserModel
        {
            get;
            set;
        }


        public string AccessToken { set; get; }

        public string RefreshToken { set; get; }

        public long AccessTokenExpire { set; get; }

        #endregion
    }
}