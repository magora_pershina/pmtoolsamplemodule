﻿using System;
using System.Collections.Generic;
using Autofac;
using Common.Abstract;
using Common.Concrete.Helpers;

namespace Common.Models
{
    public class UserModel : BaseModel
    {
        public string Color { set; get; }
        public string FName { set; get; }
        public string LName { set; get; }
        public string DisplayName { set; get; }
        public int RedmineId { set; get; }
        public int BillableTypeId { set; get; }
        public string BillableType{ set; get; }
        public string Email { set; get; }
        public string BitbucketUserName { set; get; }
        public string MobilePhone { set; get; }
        public DateTime LastLoginDate { set; get; }
        public int? DepartmentId { set; get; }
        public string Department { set; get; }
        public string Hash { set; get; }
        public decimal HourlyRate { get; set; }
        public int? UserLevelTypeId { set; get; }
        public int? SalesTypeId { set; get; }
        public string DepartmentName { get; set; }
        public int? OfficeId { get; set; }
        public DateTime? HiredDate { get; set; }
        public DateTime? DismissedDate { set; get; }
        
        public string Gravatar
        {
            get
            {
                if (Email != null)
                {
                    var hashHelper = IoC.Instance.Resolve<IHashHelper>();
                    var hash = hashHelper.GetMd5Hash(Email).ToLower();
                    return String.Format("http://www.gravatar.com/avatar/{0}", hash);
                }
                return null;
            }
        }

        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", FName, LName);
            }
        }

        public IEnumerable<int> Groups { set; get; }
        public string Password { set; get; }
    }
}
