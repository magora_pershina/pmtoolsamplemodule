﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.ApiModels.Login
{
    [DataContract]
    public class ApiAuthInfoModel
    {
        [DataMember(Name = "displayName")]
        public string DisplayName { set; get; }

        [DataMember(Name = "userId")]
        public int UserId { set; get; }

        [DataMember(Name = "permissions")]
        public IEnumerable<string> Permissions { set; get; }
    }
}
