﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.ApiModels.Login
{
    [DataContract]
    public class ApiLoginMetaRequestModel
    {
        [DataMember(Name = "deviceId")]
        public string DeviceId { set; get; }

        [DataMember(Name = "versionApp")]
        public string VersionApp { set; get; }

        [DataMember(Name = "apiKey")]
        public string ApiKey { set; get; }

        [DataMember(Name = "token")]
        public string Token { set; get; }

        [DataMember(Name = "sessionId")]
        public string SessionId { set; get; }
    }
}
