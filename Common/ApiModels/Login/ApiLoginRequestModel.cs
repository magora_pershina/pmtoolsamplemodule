﻿using System.Runtime.Serialization;

namespace Common.ApiModels.Login
{
    [DataContract]
    public class ApiLoginRequestModel
    {
        [DataMember(Name = "login")]
        public string Login { set; get; }

        [DataMember(Name = "password")]
        public string Password { set; get; }

        [DataMember(Name = "meta")]
        public ApiLoginMetaRequestModel Meta { set; get; }

    }
}
