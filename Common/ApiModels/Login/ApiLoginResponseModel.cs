﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.ApiModels.Login
{
    public class ApiLoginResponseModel
    {
        [DataMember (Name="accessToken")]
        public string AccessToken { set; get; }

        [DataMember(Name = "refreshToken")]
        public string RefreshToken { set; get; }

        [DataMember(Name = "accessTokenExpire")]
        public long AccessTokenExpire { set; get; }

        [DataMember(Name = "authInfo")]
        public ApiAuthInfoModel AuthInfo { set; get; }
    }
}
