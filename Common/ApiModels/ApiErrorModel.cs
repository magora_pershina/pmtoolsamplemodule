﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.ApiModels
{
    [DataContract]
    public class ApiErrorModel
    {
        [DataMember (Name = "code")]
        public string Code { set; get; }

        [DataMember(Name = "message")]
        public string Message { set; get; }

        [DataMember(Name = "field")]
        public string Field { set; get; }
    }
}
