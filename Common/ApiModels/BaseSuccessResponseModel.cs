﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.ApiModels
{
    [DataContract]
    public class BaseSuccessResponseModel<T>
    {
        [DataMember(Name = "data")]
        public T Data { set; get; }

        [DataMember(Name = "code")]
        public string Code { set; get; }
    }
}
