﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.ApiModels
{
    [DataContract]
    public class SessionTokenModel
    {
        [DataMember(Name = "token")]
        public string Token { set; get; }

        [DataMember(Name = "sessionId")]
        public string SessionId { set; get; }
    }
}
