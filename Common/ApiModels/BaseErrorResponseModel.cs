﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common.ApiModels
{
    public class BaseErrorResponseModel
    {
        [DataMember(Name = "errors")]
        public IEnumerable<ApiErrorModel> Errors { set; get; }

        [DataMember(Name = "code")]
        public string Code { set; get; }

        [DataMember(Name = "message")]
        public string Message { set; get; }
    }
}
