﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public class Enums
    {
        public enum e_LogLevel
        {
            Trace,
            Debug,
            Info,
            Warn,
            Error,
            Fatal
        }

        public enum e_MessageType
        {
            Error = 1,
            Success = 2,
            Warning = 3,
            AuthError = 4,
            Popup = 5
        }

        public enum RequestType
        {
            POST,
            GET,
            PUT,
            DELETE
        }

        public enum Error
        {
            [Display(Name = "001-002")]
            WrongApiKey,
            [Display(Name = "001-001")]
            ExpiredAccessToken,
            [Display(Name = "001-003")]
            ExpiredRefreshToken,
            [Display(Name = "001-004")]
            WrongToken,
            [Display(Name = "002-001")]
            MissedParameters
        }
    }
}
