﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.BLL.Common.Models.User;
using Common.ApiModels.Login;

namespace BLL.Abstract
{
    public interface IApiHelper
    {
        UserSessionModel GetUserModel(string token, string sessionId);
    }
}
