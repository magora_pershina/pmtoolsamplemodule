﻿using Autofac;
using BLL.Abstract;


namespace BLL.Concrete
{
    public sealed class Bll_IoCModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType<ApiHelper>().As<IApiHelper>();
            
        }
    }//end class
}
