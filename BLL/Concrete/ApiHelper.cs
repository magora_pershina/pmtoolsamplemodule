﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using App.BLL.Common.Models.User;
using BLL.Abstract;
using Common.ApiModels;
using Common.ApiModels.Login;
using Common.Concrete.Helpers;
using Common.Enums;
using Common.Models;
using Newtonsoft.Json;

namespace BLL.Concrete
{
    public class ApiHelper:IApiHelper
    {
        public UserSessionModel GetUserModel(string token, string sessionId)
        {
            var model = new ApiLoginRequestModel
            {
                Meta = new ApiLoginMetaRequestModel
                {
                    Token = token,
                    SessionId = sessionId,
                    ApiKey = CryptoHelper.PublicKeyBase64
                }
            };

            var url = String.Format("{0}{1}", System.Configuration.ConfigurationManager.AppSettings["ApiUrl"], "auth/token");
            var response = RequestHelper.Send(url, JsonConvert.SerializeObject(model), Enums.RequestType.POST);
            var successResponseModel = JsonConvert.DeserializeObject<BaseSuccessResponseModel<ApiLoginResponseModel>>(response);

            return new UserSessionModel
            {
                UserModel = new UserModel
                {
                    Id = successResponseModel.Data.AuthInfo.UserId,
                    DisplayName = successResponseModel.Data.AuthInfo.DisplayName
                },
                AccessToken = successResponseModel.Data.AccessToken,
                RefreshToken = successResponseModel.Data.RefreshToken,
                AccessTokenExpire = successResponseModel.Data.AccessTokenExpire
            };
        }
    }
}
