﻿using App.BLL.Common.Models;
using App.BLL.Common.Models.Api.Request.Friend;
using FluentValidation;

namespace App.BLL.Validators.Friend
{
    public class FriendInviteRequestValidator : AbstractValidator<FriendInviteRequestModel>
    {
        public FriendInviteRequestValidator()
        {
            RuleFor(expedition => expedition.text)
                .NotEmpty()
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(expedition => expedition.emails)
                .NotEmpty()
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleForEach(expedition => expedition.emails)
                .SetValidator(new EmailValidator("email"))
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);
        }
    }

    public class EmailValidator : AbstractValidator<string>
    {
        public EmailValidator(string collectionName)
        {
            RuleFor(x => x)
                .NotNull()
                .OverridePropertyName(collectionName)
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(email => email)
                .Matches(@"^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})$")
                .WithMessage(RequestResult.ConflictInvalidSymbol.Description)
                .WithState(x => RequestResult.ConflictInvalidSymbol.Code);
        }
    }

}
