﻿using Autofac;
using BLL.Validators;
using BLL.Validators.Auth;
using FluentValidation;

namespace App.BLL.Validators
{


    public sealed class ValidatorIoCModule : Module
    {
        #region Overridden Methods

        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterType(typeof(AuthRequestValidator)).Keyed<IValidator>(ValidationKeys.AuthRequest).As<IValidator>();

        }

        #endregion
    } 
}