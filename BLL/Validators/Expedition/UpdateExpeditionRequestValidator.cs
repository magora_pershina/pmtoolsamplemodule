﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.BLL.Common;
using App.BLL.Common.Models;
using App.BLL.Common.Models.Api;
using App.BLL.Common.Models.Api.Request.Expedition;

using FluentValidation;

namespace App.BLL.Validators.Expedition
{
    public class UpdateExpeditionRequestValidator : AbstractValidator<UpdateExpeditionRequestModel>
    {
        public UpdateExpeditionRequestValidator()
        {
            RuleFor(expedition => expedition.description)
                .NotEmpty()
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(expedition => expedition.hashId)
                .NotEmpty()
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(expedition => expedition.complexityTypeId)
                .GreaterThanOrEqualTo(8)
                .LessThanOrEqualTo(9)
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(expedition => expedition.endDate)
                .NotEmpty()
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            

            RuleFor(expedition => expedition.endDate)
                .GreaterThan(expedition => expedition.startDate)
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(expedition => expedition.startDate)
                .NotEmpty()
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(expedition => expedition.startDate)
                .GreaterThanOrEqualTo(DateTime.Now.UnixDateTime())
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(expedition => expedition.typeId)
                .NotEmpty()
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(expedition => expedition.typeId)
                .GreaterThanOrEqualTo(4)
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(expedition => expedition.typeId)
                .LessThanOrEqualTo(12)
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(expedition => expedition.name)
                .NotEmpty()
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(expedition => expedition.equipment)
                .NotEmpty()
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleForEach(route => route.prices)
                .SetValidator(new PriceValidator())
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);
            
        }

    }
}
