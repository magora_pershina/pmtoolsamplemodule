﻿using App.BLL.Common.Models;
using App.BLL.Common.Models.Api.Request.Expedition;
using FluentValidation;

namespace App.BLL.Validators.Expedition
{
    public class PriceValidator : AbstractValidator<PriceRequestModel>
    {
        public PriceValidator()
        {
            RuleFor(expedition => expedition.target)
                .Length(3, 100)
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(expedition => expedition.value)
                .GreaterThan(0)
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);
        }
    }
}