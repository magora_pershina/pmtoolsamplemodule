﻿
using App.BLL.Common.Models;
using App.BLL.Common.Models.Api.Request.Expedition;

using FluentValidation;

namespace App.BLL.Validators.Expedition
{
    public class FlophouseValidator : AbstractValidator<FlophouseRequestModel>
    {
        public FlophouseValidator()
        {
            
            RuleFor(expedition => expedition.length)
                .GreaterThan(0)
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);

            RuleFor(expedition => expedition.typeId)
                .GreaterThanOrEqualTo(32)
                .WithMessage(RequestResult.Conflict.Description)
                .WithState(x => RequestResult.Conflict.Code);
        }
    }
}
