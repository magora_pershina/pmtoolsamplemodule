﻿using System;
using BLL.Concrete;
using Common.ApiModels;
using Common.Concrete.Helpers;
using Common.Enums;
using FluentValidation;

namespace BLL.Validators.Auth
{

    public class AuthRequestValidator : AbstractValidator<SessionTokenModel>
    {
        public AuthRequestValidator()
        {
            var missedParameter = ErrorHelper.GetModelByEnum(Enums.Error.MissedParameters);
            var wrongToken = ErrorHelper.GetModelByEnum(Enums.Error.WrongToken);

            RuleFor(x => x.Token)
                .NotEmpty()
                .WithMessage(missedParameter.Text)
                .WithState(x => missedParameter.CodeStr);

            RuleFor(x => x.SessionId)
                .NotEmpty()
                .WithMessage(missedParameter.Text)
                .WithState(x => missedParameter.CodeStr);

            RuleFor(x => x)
                .Must(x => ValidateToken(x.Token))
                .WithMessage(wrongToken.Text)
                .WithState(x => wrongToken.CodeStr);
         }

        private bool ValidateToken(string token)
        {
            if (String.IsNullOrEmpty(token)) return false;
            
            var timestamp = Convert.ToInt64(CryptoHelper.Decryption(token));
            var date = TimeHelper.GetDateFromTimestamp(timestamp);
            return !((DateTime.Now - date).TotalSeconds >
                     Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["RequestTimeValidSeconds"]));
        }
    }
}
