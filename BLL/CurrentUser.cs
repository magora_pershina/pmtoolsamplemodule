﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using App.BLL.Common.Models.User;
using Common.Models;

namespace BLL
{
    #region Usings

    

    #endregion

    public static class CurrentUser
    {
        #region Constants

        public const string CURRENT_USER_KEY = "CurrentUSER";

        public const string UPLOADFILE_KEY = "UPLOADFILEKEY";

        #endregion

        #region Static Properties

        public static Collection<UserModel> UsersCollection = new Collection<UserModel>();

        public static UserSessionModel Info
        {
            get
            {
                return (UserSessionModel)HttpContext.Current.Session[CURRENT_USER_KEY];
            }
            set
            {
                HttpContext.Current.Session[CURRENT_USER_KEY] = value;
            }
        }

        public static bool IsLoggedIn
        {
            get
            {
                var result = HttpContext.Current.Session[CURRENT_USER_KEY] != null && !IsExpirated();

                return result;
            }
        }

        #endregion

        #region Static Methods

        public static KeyValuePair<int, KeyValuePair<int, bool>[]>[] GetAccessByGroup(int GroupID)
        {
            var res = Info != null ? Info.SystemAccess.FirstOrDefault(x => x.Key == GroupID) : new KeyValuePair<int, KeyValuePair<int, KeyValuePair<int, bool>[]>[]>();


            return res.Value ?? new KeyValuePair<int, KeyValuePair<int, bool>[]>[1];
        }
        //
        public static KeyValuePair<int, bool>[] GetAccessByGroup(int GroupID, int AccessMatrixEntityID)
        {
            var res = GetAccessByGroup(GroupID).FirstOrDefault(x => x.Key == AccessMatrixEntityID);

            return res.Value ?? new KeyValuePair<int, bool>[1];
        }
        //
        public static bool GetAccessByGroup(int GroupID, int AccessMatrixEntityID, int CrudOperation)
        {
            var res = GetAccessByGroup(GroupID, AccessMatrixEntityID).FirstOrDefault(x => x.Key == CrudOperation).Value;

            return res;
        }

        private static bool IsExpirated()
        {
            var result = false;

            Info.IsExpirated = result;

            return result;
        }

        public static void Initialize(UserSessionModel model)
        {
            Info = model;
        }



        #endregion
    } //end class
}